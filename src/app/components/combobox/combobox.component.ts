import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-combobox',
  templateUrl: './combobox.component.html',
  styleUrls: ['./combobox.component.css']
})
export class ComboboxComponent implements OnInit {

  tipo1:any;
  tipo2:any;
  tipo3: any;
  selectedTipo1: any = {
    id: 0, name: ''
  };
  selectedTipo2: any = {
    id: 0, name: ''
  }

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.showAll();
    this.onSelect(this.selectedTipo1.id);
    this.showAll3();
    this.onSelect3(this.selectedTipo2.id)
  }

  showAll(){
    this.dataService.getAll().subscribe(
      (data:any)=> {
        this.tipo1 = data,
        console.log(this.tipo1);
        
      }
    )
  }

  onSelect(tipo1_id:any){
    this.dataService.getAll().subscribe((res:any)=> {
      this.tipo2 = res['tipo2'].filter(
        (res:any)=> res.tipo1_id == tipo1_id.value
      ),
      console.log(this.tipo2);
    })
  }

  showAll3(){
    this.dataService.getAll().subscribe(
      (data:any)=> {
        this.tipo2 = data,
        console.log(this.tipo2);
        
      }
    )
  }

  onSelect3(tipo2_id:any){
    this.dataService.getAll().subscribe((rer:any)=> {
      this.tipo3 = rer['tipo3'].filter(
        (rer:any)=> rer.tipo2_id == tipo2_id.value
      ),
      console.log(this.tipo3);
    })
  }
}
